const fireadmin = require('./firebase-service');
const firestore = fireadmin.firestore();

function Staff(id, role, name, email) { //temporary here -> staff
    this.id = id;
    this.role = role;
    this.name = name;
    this.email = email;
}

exports.createStaff = function (req, res) {
    fireadmin.auth().createUser({
        email: req.body.email,
        emailVerified: false,
        disabled: false,
    })
        .then(function (userRecord) {
            // See the UserRecord reference doc for the contents of userRecord.
            console.log('Successfully created new user:', userRecord.uid);

            firestore.collection('staff').doc(userRecord.uid).set({
                name: req.body.name,
                role: req.body.role,
                email: req.body.email //added maybe also in firestore ausbessern
            }).then(function() {
                console.log("Document successfully written!");

                return res.status(200).send();
            }).catch(function(error) {
                console.error("Error writing document: ", error);

                fireadmin.auth().deleteUser(userRecord.uid)
                    .then(() => {
                        return res.status(400).send({error})
                    });
            });
        })
        .catch(function (error) {
            console.log(error);
            return res.status(400).send({error})
        });
};

exports.getAllAuthProfiles = function (req, res) { //list auth profiles
    fireadmin.auth().listUsers().   //list 1000 users a time ordered by uid
        then(function(listUserResult){
            listUserResult.users.forEach(function(userRecord){
                console.log('user: ', userRecord.toJSON());
            });
        })
        .catch(function(error){
            console.log('Error listing users:', error);
        });
};

exports.getAllStaffUsers = function (req, res) {
    const staff = [];
    firestore.collection('staff').get()
        .then(querySnapshot => {
            querySnapshot.docs.forEach(doc => {
                staff.push(new Staff(doc.id, doc.data().role, doc.data().name, doc.data().email));
                //console.log(doc.id + " " + doc.data().role + " " + doc.data().name);
                //console.log(staff);
            });
            console.log(staff);
            res.status(200).send(staff);
        })
        .catch(error => {
            console.log(error);
        });
};
