const http = require('http'),
    express = require('express'),
    firebaseController = require('./firebase-controller'),
    dotenv          = require('dotenv'),
    errorhandler    = require('errorhandler'),
    bodyParser      = require('body-parser'),
    helmet          = require('helmet'),
    dbscontroller = require('./dbs-controller.js');

var app = express();
app.use(helmet())
    
dotenv.config();
    
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});
    
if (process.env.NODE_ENV === 'development') {
    app.use(logger('dev'));
    app.use(errorhandler())
}

app.post('/createStaff', firebaseController.createStaff);
app.get('/listAllAuthProfiles', firebaseController.getAllAuthProfiles);
app.post('/listAllStaffProfiles', firebaseController.getAllStaffUsers);
app.get('/getReports', dbscontroller.getReports);

var port = 5000;
var server = http.createServer(app);
 
server.listen(port, function (err) {
  console.log('listening in http://localhost:' + port);
});