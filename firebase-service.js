var admin = require("firebase-admin");

var serviceAccount = require("./topit-supportcenter-firebase-adminsdk-0m380-b494e74b0c");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://topit-supportcenter.firebaseio.com"
});

module.exports = admin;